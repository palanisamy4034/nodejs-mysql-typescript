# Product Information System

    

## Dependencies
#### Backend Dependencies
    * ExpressJS
    * Mongoose
    * mLab
    * TypeScript
    * concurrently


#### Installation and Setup
* Navigate to a directory of choice
* Clone this repo
    ```sh

    ```
* Navigate to the repo's folder
    ```sh
    cd nodejs-mongodb
    ```
* Install app dependencies
    ```sh
    npm install
    ```
* Run the server
    ```sh
    npm run start-server
    ```
* Deploying app for production
    ```sh
    npm run start-build
    ```

## Introduction
* **Product Information System** 

#### It has the following features
    * Create a User
    * Edit a User
    * Delete a User
    * List User
    * Create a Product
    * Edit a Product
    * Delete a Product
    * List Products
