import mongoose from "mongoose";
import appConfig from "./config/config";
import SocketDetails from "./models/socketActiveConnection";

class DataBaseConnection {
  public state: any = {
    db: null,
    mode: null
  };

  public async connect() {
    const url = appConfig.config.Database.URL;
    mongoose.set("useCreateIndex", true);
    mongoose.set("debug", true);
    mongoose.set("useFindAndModify", false);
    const db = await mongoose.connect(url, { useNewUrlParser: true }).then( async () => {
        await SocketDetails.updateMany({isActive : true}, { isActive : false });
         // tslint:disable-next-line:no-console
        console.log("DB connsected");
      },
      (err) => {
        // console.log("MongoDB connection error. Please make sure MongoDB is running.");
        // console.log(err);
        process.exit();
      }
    );
    return true;
  }

  public get() {
    return this.state.db;
  }

}
export default new DataBaseConnection();
