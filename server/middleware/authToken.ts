import jwt from "jsonwebtoken";

export const validateToken = (req, res, next) => {
    const token = req.body.token || req.query.token || req.headers.authorization;
    if (token) {
      // tslint:disable-next-line:only-arrow-functions
      jwt.verify(token, "palani", function(err, decoded) {
          if (err) {
              return res.status(401).send({ success: false, message: "Failed to authenticate token / Unauthorized" });
            } else {
                req.decoded = decoded;
                next();
            }
      });
    } else {
      return res.status(403).send({
          message: "No token provided.",
          success: false,
      });

    }
};
