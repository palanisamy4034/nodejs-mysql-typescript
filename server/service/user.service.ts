import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";
import * as uuid from "uuid";
import { ErrorResponse, SucessResponse } from "../_helpers/response.header";
import { emailValidate } from "../_helpers/util.helper";
import User from "../models/userHdr";

export class UserService {

  public getAllUsers = async (req) => {
    const user = await User.find({ userId: { $ne: req } }).select("-_id -userPassword -__v");
    return new SucessResponse(user);
  }

  public authendicateUser = async (req) => {
    const existUser: any = await User.findOne({ userEmail: { $regex:
      new RegExp("^" + req.body.userEmail.toLowerCase(), "i") }});
    if (existUser !== null) {
      const match = await bcrypt.compare((req.body.userPassword).toString(), existUser.userPassword);
      if (match) {
        try {
          const token = await jwt.sign({ data: existUser.userId }, "palani", { expiresIn: 60 * 60 });
          const authResponse = { ...existUser._doc, ...{ authToken: token } };
          delete authResponse.userPassword;
          return new SucessResponse(authResponse);
        } catch (err) {
          return new ErrorResponse(existUser);
        }
      } else {
        return new ErrorResponse({ msg: "InValid Password" });
      }
    }
    return new ErrorResponse({ msg: "User Not fround" });
  }

  public createUser = async (request) => {
    if (emailValidate(request.body.userEmail)) {
      const body = request.body;
      body.userId = uuid.v1();
      const existUser = await User.findOne({ userEmail: request.body.userEmail });
      if (existUser === null) {
        const user = new User(body);
        const newUser = await user.save();
        return new SucessResponse(newUser, 201);
      }
      return new SucessResponse({ msg: ` Email Already exits ${request.body.userEmail}` });
    } else {
      return new ErrorResponse({ msg: `Invalid email ${request.body.userEmail}` });
    }
  }
}
