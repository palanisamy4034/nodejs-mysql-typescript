
import SocketDetails from "../../models/socketActiveConnection";

export class SocketService {

    public postSocketDetails = async (req) => {
        const socketDtl = new SocketDetails(req);
        const newMgs = await socketDtl.save();
        return newMgs;
    }

    public updateSocketDetails = async (req) => {
        const time = new Date();
        const socketDtl = await SocketDetails.findOneAndUpdate({socketId: req}, {isActive: false, updatedAt: time});
        return socketDtl;
    }

    public getUserActive = async (req) => {
        return await SocketDetails.find({isActive: true});
    }

    public getSocketIdByUserList = async (users) => {
        const socketIds = await SocketDetails.find({userId: {$in: users}, isActive: true}).select("socketId");
        return socketIds.map((e: any) => e.socketId);
    }
}
