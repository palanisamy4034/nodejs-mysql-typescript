
import _ from "lodash";
import uuid from "uuid";
import { SucessResponse } from "./../../_helpers/response.header";
import chatConvSchema from "./../../models/chatConvModel";
import chatMessage from "./../../models/chatMessage";
import { SocketService } from "./socketService";

export class ChatMessageService {

    public getAllChats = async (req) => {
        const convHdr: any = await chatConvSchema.find({ recipient: { $in: [req] } }).select("recipient roomId -_id");
        const transScriptList: object[] = [];
        for (const ele of convHdr) {
            const msg = await this.getMessage(ele, req);
            if (msg.length > 0) {
                const transScript: object = { messages: msg, recipient: ele.recipient,
                    roomId: ele.roomId, unReadChat: 0 };
                transScriptList.push(transScript);
            }
        }
        return new SucessResponse(transScriptList);
    }

    public authendicateUser = (req) => {
        return chatMessage.find({ UserName: req.body.UserName }, (err, data) => {
            return data;
        });
    }

    public postChatmessage = async (req) => {
        const chatMsg = new chatMessage(req);
        const newMgs = await chatMsg.save();
        const recipient: any = await chatConvSchema.findOne({roomId: req.roomId}).select("recipient");
        const socketIds = await new SocketService().getSocketIdByUserList(recipient.recipient);
        return [newMgs, socketIds];
    }

    public createConvId = async (req) => {
        const body = req.body;
        const data = await chatConvSchema.findOne({ roomType: body.roomType, recipient: { $all: body.recipient } });
        if (data !== null) {
            return new SucessResponse(data);
        }
        body.roomId = uuid.v1();
        const newConvId = await new chatConvSchema(body).save();
        return new SucessResponse(newConvId);
    }

    public getConvId = (req) => {
        return chatConvSchema.find({ roomId: req }, (err, data) => {
            return new SucessResponse(data);
        });
    }

    public getAllConv = async (req) => {
        const roomIds = await chatConvSchema.find({ recipient: { $in: [req] } }).select("_id");
        const trans = await chatMessage.find({ roomId: roomIds.map((e) => e._id) });
        return new SucessResponse(trans);
    }

    public deleteMessageById = async (body) => {
        // const body = req.body;
        const chat = await chatMessage.findOneAndUpdate({ _id: body._id },
                { $push: { deleteByUser: body.userId } });
        return new SucessResponse(chat);
    }

    private async getMessage(ele: any, req: any) {
        return await chatMessage.find({ roomId: ele.roomId, deleteByUser: { $nin: [req] } });
    }
}
