import mongoose from "mongoose";
const Schema = mongoose.Schema;

const SocketDetails = new Schema({
    socketId: String,
    userId: String,
    // tslint:disable-next-line:object-literal-sort-keys
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now},
    isActive: {type: Boolean, default: true}
});

export default mongoose.model("socketDetails", SocketDetails);
