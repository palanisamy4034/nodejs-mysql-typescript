import * as mongoose from "mongoose";
const Schema = mongoose.Schema;
const roomType = ["direct", "group", "share"];
const chatConvSchema = new Schema({
    $key: String,
    roomId: String,
    roomType: { type: String, required : true , enum: roomType , default : "direct"},
    // tslint:disable-next-line:object-literal-sort-keys
    recipient: [],
    conversations: []

});

export default mongoose.model("chatConvHdr", chatConvSchema);
