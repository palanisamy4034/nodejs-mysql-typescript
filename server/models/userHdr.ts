import * as bcrypt from "bcrypt";
import * as mongoose from "mongoose";
const SALT_WORK_FACTOR = 10;
const Schema = mongoose.Schema;

const UserDtlSchema = new Schema({
  key: String,
  userId: {
    type: String,
    unique: true
  },
  userName: {
    type: String,
    required: true
  },
  userCategory: String,
  userImageUrl: String,
  favourite: Boolean,
  userEmail: {
    type: String,
    unique: true
  },
  userMobile: Number,
  userPassword: {
    type: String,
    required: true
  }
});

UserDtlSchema.pre("save", function(next) {
  const user: any = this;
  if (!user.isModified("userPassword")) { return next(); }
  // tslint:disable-next-line:only-arrow-functions
  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
      if (err) { return next(err); }
      // hash the password using our new salt
      // tslint:disable-next-line:only-arrow-functions
      bcrypt.hash(user.userPassword, salt, function(error, hash) {
          if (err) { return next(error); }
          user.userPassword = hash;
          next();
      });
  });
});

UserDtlSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.userPassword, function(err, isMatch) {
      if (err) { return cb(err); }
      cb(null, isMatch);
  });
};
// export default mongoose.model('Student', StudentSchema)
export default mongoose.model("users", UserDtlSchema);
