import mongoose from "mongoose";
const Schema = mongoose.Schema;
const ChatMessageDtlSchema = new Schema({
    $key: String,
    roomId: String,
    // tslint:disable-next-line:object-literal-sort-keys
    msgType: String,
    eventTime: {type: Date, default: Date.now},
    payload: String,
    mediaPath: String,
    status: String,
    direction: String,
    reference: String,
    scheduledAt: String,
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now},
    recipient: [],
    isActive: {type: Boolean, default: true},
    deleteByUser: Array,
    sender: {
        avatarUrl: String,
        displayName: String,
        userId:  String,
    }

});

export default mongoose.model("messageTrans", ChatMessageDtlSchema);
