import dotenv from "dotenv";
import express from "express";
import path from "path";
// tslint:disable-next-line:max-line-length
import { AuthController, ChatMessageController, HttpClientController, SocketController, UsersController } from "./controllers/index";
import DataBaseConnection from "./db";
import * as auth from "./middleware/authToken";
// tslint:disable-next-line:no-var-requires
const addon = require("bindings")("addon.node");
class App {

  public express: express.Application;
  // public routePrv: Routes = new Routes();

  constructor() {
    this.setEnvironment();
    this.database();
    this.express = express();
    this.middleware();
    this.initializeControllers(this.InitRoute());
  }

  /**
   * database connection
   */
  private async database() {
    await DataBaseConnection.connect();
  }

  /**
   * http(s) request middleware
   */
  private middleware(): void {
    this.express.set("view engine", "pug");
    this.express.set("views", path.join(__dirname, "views"));
    this.express.use(express.urlencoded());
    this.express.use(express.json());
    this.express.use((req, res, next) => {
      res.header("Access-Control-Allow-Origin", "*"); // dev only
      res.header("Access-Control-Allow-Methods", "OPTIONS,GET,PUT,POST,DELETE");
      res.header("Access-Control-Allow-Headers", "Content-Type, Authorization, x-client-id");
      if (req.method === "OPTIONS") {
        res.status(200).send();
      } else {
        next();
      }
    });
  }

  /**
   * app environment configuration
   */
  private setEnvironment(): void {
    dotenv.config({ path: ".env" });
  }

  /**
   * API main routes
   */
  private initializeControllers(controllers) {

    this.express.get("/pug", (req, res) => {
      res.render("index", { title: "Hey", message: addon.add(3, 5) });
    });
    this.express.use("/", new AuthController().router, new HttpClientController().router);
    controllers.forEach((controller) => {
      this.express.use("/", auth.validateToken, controller.router);
    });

    this.express.get("/pug", (req, res) => {
      res.render("index", { title: "Hey", message: "Hello there!" });
    });

    this.express.use("*", (req, res) => {
      res.status(404).send({ error: `\/${req.baseUrl}\/ doesn't exist` });
    });
  }

  private InitRoute() {
    return [new SocketController(), new UsersController(), new ChatMessageController()];
  }

}

export default new App().express;
