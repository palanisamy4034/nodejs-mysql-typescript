import _res from "./../_helpers/response.helper";
import { SocketService } from "./../service/chatService/socketService";

import * as express from "express";

export class SocketController {
  public path = "/socket";
  public router = express.Router();

  constructor() {
    this.intializeRoutes();
  }

  public intializeRoutes() {
    this.router.get(this.path, this.getSocketDetailsById);
  }

  public getSocketDetailsById = async (request: express.Request, response: express.Response) => {
    const data = await new SocketService().getUserActive(request);
    // console.log(data);
    return response.send(data);
  }

  public addUserSocketDetails = (userId, socketId) => {
    const data = new SocketService().postSocketDetails({ socketId, userId });
  }

  public disconnectEvent = async (socketId) => {
    const data = await new SocketService().updateSocketDetails(socketId);
    return true;

  }

}
