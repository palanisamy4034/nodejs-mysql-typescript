import { Request, Response, Router } from "express";
import multer from "multer";
import User from "../models/userHdr";
import { UserService } from "../service/user.service";
import { CloudinaryUpload } from "./../_helpers/cloudinary.upload";
import _res from "./../_helpers/response.helper";
const upload = multer({ dest: "./public/images/uploads/" });
export class UsersController {
  public path = "/user";
  public router = Router();
  public multipartMiddleware: any;

  constructor() {
    this.intializeRoutes();
  }

  public intializeRoutes() {
    this.router.get(`${this.path}/contacts/:id`, this.getAllUsers);
    this.router.get(`${this.path}/:id`, this.getUserById);
    this.router.post(`${this.path}/update`, this.updateUserById);
    this.router.post(`${this.path}/uploadImage`, upload.any(), this.uploadImage);
  }

  public getAllUsers = async (request: Request, response: Response) => {
    const userService = new UserService();
    try {
      const user = await userService.getAllUsers(request.params.id);
      return _res.statusOk(request, response, user);
    } catch (error) {
      return response.send(error);
    }
  }

  public updateUserById = (request: Request, response: Response, next) => {
    const user = (request.body);
    const options = { upsert: true, returnNewDocument : true };
    User.findOneAndUpdate({userId: request.body.userId}, { $set: user}, options, (err, updatedUser) => {
      if (err) {
        response.send(err);
      } else if (!updatedUser) {
        response.send(400);
           } else {
        response.send(updatedUser);
           }
    });
  }

  public getUserById = (request: Request, response: Response, next) => {
    User.findOne({ userId: request.params.id }, (err, user) => {
      if (err) {
        response.send(err);
      } else if (!user) {
        response.send(400);
           } else {
        response.send(user);
           }
    });
  }

  public uploadImage = async (request: Request, response: Response, next) => {
    try {
      const cloud = await new CloudinaryUpload().uploadImage(request, "user");
      return cloud;
    } catch (err) {
      return response.send(err);
    }
  }
}
