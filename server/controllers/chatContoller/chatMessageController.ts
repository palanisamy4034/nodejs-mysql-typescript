import express = require("express");
import { Request, Response } from "express";
import { ChatMessageService } from "../../service/chatService/chatMessageService";
import { WebsocketService } from "../../websocket/websocket";
import _res from "./../../_helpers/response.helper";
export class ChatMessageController {
    public path = "/chat";
    public router = express.Router();

    constructor() {
        this.intializeRoutes();
    }

    public intializeRoutes() {
        this.router.get(`${this.path}`, this.getAllchats);
        this.router.post(`${this.path}/createConvId`, this.createConvId);
        this.router.get(`${this.path}/createConvId`, this.getAllConv);
        this.router.get(`${this.path}/sendmessage`, this.sendMessage);
        this.router.post(`${this.path}/delete`, this.deleteMessageById);
    }

    public postChatMsg = async (msg: any, io: any) => {
        const chatmsgService = new ChatMessageService();
        try {
            const msgs: any[] = await chatmsgService.postChatmessage(msg);
            msgs[1].forEach((ele) => {
                WebsocketService.io.to(ele).emit("messagerecive", msgs[0]);
            });
            // io.emit(_msg);
        } catch (error) {
            throw error;
        }
    }

    public getAllchats = async (request: Request, response: Response) => {
        try {
            const user = await this.getChatByUser(request.headers["x-client-id"]);
            return _res.statusOk(request, response, user);
        } catch (error) {
            return response.send(error);
        }
    }

    public getChatByUser = async (userId) => {
        const chatmsgService = new ChatMessageService();
        const conv = await chatmsgService.getAllChats(userId);
        return conv;
    }

    public createConvId = async (request: Request, response: Response) => {
        try {
            const msg = await new ChatMessageService().createConvId(request);
            return _res.statusOk(request, response, msg);
        } catch (error) {
            return response.send(error);
        }
    }

    public getAllConv = async (request: Request, response: Response) => {
        const chatmsgService = new ChatMessageService();
        try {
            const user = await chatmsgService.getAllConv(request);
            return _res.statusOk(request, response, user);
        } catch (error) {
            return response.send(error);
        }
    }

    private sendMessage = async (request: Request, response: Response) => {
        const chatmsgService = new ChatMessageService();
        try {
            const msg = await chatmsgService.postChatmessage(request.body);
            return _res.statusOk(request, response, msg);
        } catch (error) {
            return response.send(error);
        }
    }

    private deleteMessageById = async (request: Request, response: Response) => {
        const chatmsgService = new ChatMessageService();
        try {
            const msg = await chatmsgService.deleteMessageById(request.body);
            return _res.statusOk(request, response, msg);
        } catch (error) {
            return response.send(error);
        }
    }
}
