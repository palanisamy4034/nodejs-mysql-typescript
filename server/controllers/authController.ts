import { Request, Response, Router } from "express";
import { ErrorResponse } from "../_helpers/response.header";
import { UserService } from "../service/user.service";
import _res from "./../_helpers/response.helper";
import { BaseController } from "./baseControllers";

export class AuthController extends BaseController{
    public path = "/auth";
    public router = Router();
    public multipartMiddleware: any;

    constructor() {
        super();
        this.intializeRoutes();
    }

    public intializeRoutes() {
        this.router.post(`${this.path}/login`, this.validateUser);
        this.router.post(`${this.path}/signup`, this.createUser);
    }

    private validateUser = async (request: Request, response: Response, next) => {
        const userService = new UserService();
        try {
            const user = await userService.authendicateUser(request);
            return _res.statusOk(request, response, user);
        } catch (err) {
            return response.send(err);
        }
    }

    private createUser = async (request: Request, response: Response, next) => {
        try {
            const res = await new UserService().createUser(request);
            return _res.statusOk(request, response, res);
        } catch (error) {
            return _res.statusOk(request, response, new ErrorResponse(error));
        }
    }
}
