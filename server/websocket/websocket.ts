import * as _ from "lodash";
import { ChatMessageController } from "./../controllers/chatContoller/chatMessageController";
import { SocketController } from "./../controllers/socket.controller";
export class UserList {
    public userId!: string;
    public socketId!: string;
}
// tslint:disable-next-line:max-classes-per-file
export class WebsocketService {
    public static io;
    public userList: UserList[] = [];
    public connectionSubscribe(server) {
        WebsocketService.io = require("socket.io")(server);
        WebsocketService.io.on("connection", (socket) => {
            socket.on("join", (user) => {
                const users = new SocketController().addUserSocketDetails(user, socket.id );
            // WebsocketService.io.to(socket.id).emit('messagerecive', `${users}`);

            });
            socket.on("message", (user) => {
                const io = new ChatMessageController().postChatMsg(user, socket);
            });
            socket.on("notification", (user) => {
                // tslint:disable-next-line:no-console
                console.log(user);
            });
            socket.on("newuser", (user) => {
                // tslint:disable-next-line:no-console
                console.log(user);
            });
            socket.on("disconnect", () => {
                const idx = _.findIndex(this.userList, { socketId: socket.id });
                this.userList.splice(idx, 1);
                const users = new SocketController().disconnectEvent(socket.id);
            });
        });
    }
}
// https://socket.io/docs/emit-cheatsheet/
