import * as cloudinary from "cloudinary";
import * as uuid from "uuid";
import appConfig from "./../config/config";

const config = appConfig.config;
export class CloudinaryUpload {
    public async uploadImage(req, userId) {
        cloudinary.config({
            api_key: config.Cloudinary.api_key,
            api_secret: config.Cloudinary.api_secret,
            cloud_name: config.Cloudinary.cloud_name,
        });
        const path = `${userId}/${uuid.v1()}`;
        return await cloudinary.v2.uploader.upload(
            req.files[0].path,
            {
                public_id: path,
                crop: "limit",
                width: 2000,
                height: 2000,
            },
            (error: any, result: any) => {
                // rimraf.sync("./public/images/uploads/");
                return result || error;
            }
        );
    }

}

/** Future use */
// eager: [
//     {
//         width: 200, height: 200, crop: 'thumb', gravity: 'face',
//         radius: 20, effect: 'sepia'
//     },
//     { width: 100, height: 150, crop: 'fit', format: 'png' }]
