export const ResponseCode = {
    ACCEPTED: {
        code: "202",
        statusText: "Accepted"
    },
    ALREADY_REPORTED: {
        code: "208",
        statusText: "Already Reported"
    },
    BAD_GATEWAY: {
        code: "502",
        statusText: "Bad Gateway"
    },
    BAD_REQUEST: {
        code: "400",
        statusText: "Bad Request"
    },
    BANDWIDTH_LIMIT_EXCEEDED: {
        code: "509",
        statusText: "Bandwidth Limit Exceeded"
    },
    BLOCKED_BY_PARENTAL_CONTROLS: {
        code: "450",
        statusText: "Blocked by Windows Parental Controls"
    },
    CLIENT_CLOSED_REQUEST: {
        code: "499",
        statusText: "Client Closed Request"
    },
    CONFLICT: {
        code: "409",
        statusText: "Conflict"
    },
    CREATED: {
        code: "201",
        statusText: "Created"
    },
    ENHANCE_YOUR_CALM: {
        code: "420",
        statusText: "Enhance your calm"
    },
    EXPECTATION_FAILED: {
        code: "417",
        statusText: "Expectation Failed"
    },
    FAILED_DEPENDENCY: {
        code: "424",
        statusText: "Failed Dependency"
    },
    FORBIDDEN: {
        code: "403",
        statusText: "Forbidden"
    },
    GATEWAY_TIMEOUT: {
        code: "504",
        statusText: "Gateway Timeout"
    },
    GONE: {
        code: "410",
        statusText: "Gone"
    },
    HTTP_VERSION_NOT_SUPPORTED: {
        code: "505",
        statusText: "HTTP Version Not Supported"
    },
    INSUFFICIENT_SPACE_ON_RESOURCE: {
        code: "419",
        statusText: "Insufficient Space on Resource"
    },
    INSUFFICIENT_STORAGE: {
        code: "507",
        statusText: "Insufficient Storage"
    },
    INTERNAL_SERVER_ERROR: {
        code: "500",
        statusText: "Server Error"
    },
    LENGTH_REQUIRED: {
        code: "411",
        statusText: "Length Required"
    },
    LOCKED: {
        code: "423",
        statusText: "Locked"
    },
    LOOP_DETECTED: {
        code: "508",
        statusText: "Loop Detected"
    },
    METHOD_NOT_ALLOWED: {
        code: "405",
        statusText: "Method Not Allowed"
    },
    MISDIRECTED_REQUEST: {
        code: "421",
        statusText: "Misdirected Request"
    },
    MOVED_PERMANENTLY: {
        code: "301",
        statusText: "Moved Permanently"
    },
    MOVED_TEMPORARILY: {
        code: "302",
        statusText: "Moved Temporarily"
    },
    MULTI_STATUS: {
        code: "207",
        statusText: "Multi-Status"
    },
    MULTIPLE_CHOICES: {
        code: "300",
        statusText: "Multiple Choices"
    },
    NETWORK_AUTHENTICATION_REQUIRED: {
        code: "511",
        statusText: "Network Authentication Required"
    },
    NETWORK_READ_TIMEOUT: {
        code: "598",
        statusText: "Network Read Timeout Error"
    },
    NETWORK_CONNECT_TIMEOUT: {
        code: "599",
        statusText: "Network Connect Timeout Error"
    },
    NO_CONTENT: {
        code: "204",
        statusText: "No Content"
    },
    NO_RESPONSE: {
        code: "444",
        statusText: "No Response"
    },
    NON_AUTHORITATIVE_INFORMATION: {
        code: "203",
        statusText: "Non Authoritative Information"
    },
    NOT_ACCEPTABLE: {
        code: "406",
        statusText: "Not Acceptable"
    },
    NOT_EXTENDED: {
        code: "510",
        statusText: "Not Extended"
    },
    NOT_FOUND: {
        code: "404",
        statusText: "Not Found"
    },
    NOT_IMPLEMENTED: {
        code: "501",
        statusText: "Not Implemented"
    },
    NOT_MODIFIED: {
        code: "304",
        statusText: "Not Modified"
    },
    OK: {
        code: "200",
        statusText: "OK"
    },
    PARTIAL_CONTENT: {
        code: "206",
        statusText: "Partial Content"
    },
    PAYMENT_REQUIRED: {
        code: "402",
        statusText: "Payment Required"
    },
    PERMANENT_REDIRECT: {
        code: "308",
        statusText: "Permanent Redirect"
    },
    PRECONDITION_FAILED: {
        code: "412",
        statusText: "Precondition Failed"
    },
    PRECONDITION_REQUIRED: {
        code: "428",
        statusText: "Precondition Required"
    },
    PROCESSING: {
        code: "102",
        statusText: "Processing"
    },
    PROXY_AUTHENTICATION_REQUIRED: {
        code: "407",
        statusText: "Proxy Authentication Required"
    },
    REQUEST_HEADER_FIELDS_TOO_LARGE: {
        code: "431",
        statusText: "Request Header Fields Too Large"
    },
    REQUEST_TIMEOUT: {
        code: "408",
        statusText: "Request Timeout"
    },
    REQUEST_TOO_LONG: {
        code: "413",
        statusText: "Request Entity Too Large"
    },
    REQUEST_URI_TOO_LONG: {
        code: "414",
        statusText: "Request-URI Too Long"
    },
    REQUESTED_RANGE_NOT_SATISFIABLE: {
        code: "416",
        statusText: "Requested Range Not Satisfiable"
    },
    RETRY_WITH: {
        code: "449",
        statusText: "Retry With"
    },
    RESET_CONTENT: {
        code: "205",
        statusText: "Reset Content"
    },
    SEE_OTHER: {
        code: "303",
        statusText: "See Other"
    },
    SERVICE_UNAVAILABLE: {
        code: "503",
        statusText: "Service Unavailable"
    },
    SWITCHING_PROTOCOLS: {
        code: "101",
        statusText: "Switching Protocols"
    },
    TEMPORARY_REDIRECT: {
        code: "307",
        statusText: "Temporary Redirect"
    },
    TOO_MANY_REQUESTS: {
        code: "429",
        statusText: "Too Many Requests"
    },
    UNAUTHORIZED: {
        code: "401",
        statusText: "Unauthorized"
    },
    UNAVAILABLE_FOR_LEGAL_REASONS: {
        code: "451",
        statusText: "Unavailable For Legal Reasons"
    },
    UNSUPPORTED_MEDIA_TYPE: {
        code: "415",
        statusText: "Unsupported Media Type"
    },
    USE_PROXY: {
        code: "305",
        statusText: "Use Proxy"
    },
    UPGRADE_REQUIRED: {
        code: "426",
        statusText: "Upgrade Required"
    }
};
